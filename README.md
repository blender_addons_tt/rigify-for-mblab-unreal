# Rigify-for-MB-Lab-tt


This is a modified copy of [Sync X's version](https://github.com/Core-SyncX/Rigify-for-MB-Lab-Sync-X-) which is in turn based on [Daniel Engler's version](https://github.com/danielenger/Rigify-for-MB-Lab/tree/master).

It allows you to use a Rigify like rig to animate a MBLab character. This version is adpated for use with Unreal Engine 5 and potentially other amature/skeleton driven software.

# New Features

There's a new operator called `Link Armature to Rig`, it allows the Rigify rig to control the MB-Lab armature which in turn controls the mesh.

This is useful for working with armature/skeleton driven software such as Unreal Engine. It's also quicker and more convenient than the original workflow.

There's a new option for `Generate Rig`, called `Fix Backwards Legs` it's enabled by default and fixes the backwards facing legs that we get with this method (presumably caused by either MB-Lab or Rigify for MB-Lab using the wrong bone.roll) which you shouldn't see thanks to this feature, unless of course you turn it off. Currently it uses hardcoded values so your mileage may vary, this will change soon.

This way we get to use benefits of the rigify rig in Blender and the MBLab rig in Unreal.


# Usage

1. Download or clone the Rigify-for-MB-Lab add-on
2. Install it by copying the directory to Blender's add-on directory or by using the blender zip installer
3. Enable the Rigify add-on in blender. It comes with blender by default
4. Follow the tutorial below on how to use it with MB-Lab Character

https://www.youtube.com/watch?v=JXzxVerR2C4


The video is out of date, here's the workflow:

1. Create an MBLab character.
2. Finalise the character.
3. Select the armature.
4. Click Add Meta Rig
5. Click Generate Rig
6. Select the armature again
7. Click Link Armature to Rig
8. Start animating


# Planned features

Copy bone.roll fron the MB-Lab armature.

Operator to export character/animations as .fbx with suitable settings for Unreal Enigine 5.

# Bugs/Feature Requests/etc

Please open an issue and provide as much useful information as possible.

