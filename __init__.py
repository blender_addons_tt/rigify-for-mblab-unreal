#    Rigify for MB-Lab
#    Copyright (C) 2019 Daniel Engler

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


# Thanks for help and support:
# Amir https://github.com/amirpavlo
# Alexander Gavrilov https://github.com/angavrilov


bl_info = {
    "name": "Rigify for MB-Lab",
    "description": "Rigify for MB-Lab",
    "author": "Daniel Engler, Sync X, twistedturtle",
    "version": (0, 7, 0),
    "blender": (4, 0, 0),
    "location": "View3D > Tools > Rigify for MB-Lab",
    "category": "Characters"
}


'''
Add all modules here. They must come after any modules they're
dependant upon, for instance "panel" should come last as it relies
upon the others being registered.

Each module should have a register and unregister function
unless it's a "helper" module (ie blender doesn't call any code directly)

If it's a helper module you should still put it in this list,
as Blender will then reload it along with everything else.
In other words, changes will be detected and used
when you "Reload Scripts" in Blender
'''
moduleNames = [
    "add_rig",
    "generate_rig",
    "rename_vertex_groups",
    "panel"
]


##########################################################################
# Don't edit below here...unless you know what you're doing
##########################################################################

import sys
import importlib
import os

moduleFullNames = {}
# Get proper names for the modules
for currentModuleName in moduleNames:
    if 'DEBUG_MODE' in sys.argv:
        moduleFullNames[currentModuleName] = ('{}'.format(currentModuleName))
    else:
        moduleFullNames[currentModuleName] = ('{}.{}'.format(__name__, currentModuleName))

# Reload the modules, so changes are detected
for currentModuleFullName in moduleFullNames.values():
    if currentModuleFullName in sys.modules:
        importlib.reload(sys.modules[currentModuleFullName])
    else:
        globals()[currentModuleFullName] = importlib.import_module(currentModuleFullName)
        setattr(globals()[currentModuleFullName], 'moduleNames', moduleFullNames)


# Register everything that needs registering
def register():
    for currentModuleName in moduleFullNames.values():
        if currentModuleName in sys.modules:
            if hasattr(sys.modules[currentModuleName], 'register'):
                sys.modules[currentModuleName].register()

# Make a pizza
def unregister():
    for currentModuleName in moduleFullNames.values():
        if currentModuleName in sys.modules:
            if hasattr(sys.modules[currentModuleName], 'unregister'):
                sys.modules[currentModuleName].unregister()


if __name__ == "__main__":
    register()
